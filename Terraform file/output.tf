output "instance" {
  value = "${aws_instance.example.*.public_ip}"
}
output "mysql" {
  value = "${aws_db_instance.sql.endpoint}"
}

output "elb_dns_name" {
  value = "${aws_elb.example.dns_name}"
}

