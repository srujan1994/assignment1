variable "AWS_REGION" {
  default = "eu-west-1"
}
variable "public_key_path" {
  default     = "/vagrant/aws_srujan.pem"
}
variable "key_name" {
  default = "aws_srujan"
}
variable "AMIS" {
  default =  "ami-04facb3ed127a2eb6"
 }
variable "MYSQL_PASSWORD" { }

variable "aws_availability_zones" {
  default = "eu-west-1"
}
variable "aws_security"{
  default = ["aws_secuirty_group.example-instance.id","aws_security_group.instance1.id"]
}

variable "count" {
    default = 2    ### 2 ec2 instances we are creating
}
### This is over1

