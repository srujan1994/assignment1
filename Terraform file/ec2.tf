resource "aws_instance" "example" {
  ami                      = "${var.AMIS}"     
  count                    = "${var.count}"   
  key_name                 = "${var.key_name}"
  subnet_id                = ["${aws_subnet.main-public-1.id}"]
  vpc_security_group_ids  = ["${aws_security_group.example-instance.id}"]
  user_data                = "${file("project.sh")}"
  source_dest_check        = false
  instance_type            = "t2.micro"
tags {
    Name = "${format("web-%d", count.index + 1)}"
  }
}
##
