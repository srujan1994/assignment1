
#security Group for ELB
resource "aws_security_group" "elb" {
  name = "terraform-example-elb"
  vpc_id       = "${aws_vpc.main.id}"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
### Creating ELB
resource "aws_elb" "example" {
  name                = "terraform-asg-example"
  security_groups     = ["${aws_security_group.elb.id}"]
  subnets            = ["${aws_subnet.main-public-1.id}","${aws_subnet.main-public-2.id}"]
  instances           = ["${aws_instance.example.*.id}"]
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout           = 3
    interval          = 30
    target            = "HTTP:80/mediawiki"
  }
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = "80"
    instance_protocol = "http"
  }
}

