#!/bin/bash
sudo -i
yum install -y httpd php*
yum install -y wget 
systemctl enable httpd
cd /tmp/
wget https://releases.wikimedia.org/mediawiki/1.33/mediawiki-1.33.0.tar.gz
wget https://releases.wikimedia.org/mediawiki/1.33/mediawiki-1.33.0.tar.gz.sig
cd /var/www
tar -zxf /tmp/mediawiki-1.33.0.tar.gz
ln -s mediawiki-1.33.0/ mediawiki
sed -i 's@\/var\/www\/html@\/var\/www@g' /etc/httpd/conf/httpd.conf
sed -i 's@DirectoryIndex\ index.html@DirectoryIndex\ index.html\ index.html.var\ index.php@g' /etc/httpd/conf/httpd.conf
chown -R apache:apache /var/www/mediawiki-1.33.0
chown -R apache:apache /var/www/mediawiki
service httpd restart
